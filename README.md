# zabbix-ci-docker

These images are based on `zabbix/zabbix-web-nginx-pgsql`. The difference is
that this image is meant to run stand-alone without a server for CI purposes
where you want to communicate with the web-frontend without needing a server.

The upstream image relies on the server to setup the database, but this image
will do the database initialization itself. The data is very minimal: None of
the default groups, templates, hosts, media types, etc are created.

Other than these differences, the usage is the same as the upstream image. See
the [readme][] for details how to use this image.

[readme]:https://hub.docker.com/r/zabbix/zabbix-web-nginx-pgsql

## API Key

Besides the standard login credentials (`Admin:zabbix`), an API key is setup
that can be used to authenticate against the API:

`5db357b158fb2269cebb91045043c9ad21cb05c4c924c731b6503a500efd3c99`

## Server dummy

The `alpinelinux/zabbix-ci:dummy-server-latest` image provides a service that
responds to the server health-check requests from the front-end. This is
optional and the goal is to get rid of the 'Zabbix server is not running'
message that appears if there is no running server.

To use it, start the container and set `ZBX_SERVER_HOST` on the front-end
containers. The service listens on the default zabbix server port (10051).
