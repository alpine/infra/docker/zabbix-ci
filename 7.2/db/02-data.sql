START TRANSACTION;
INSERT INTO role (roleid,name,type,readonly) VALUES ('1','User role','1','0'),
('2','Admin role','2','0'),
('3','Super admin role','3','1'),
('4','Guest role','1','0');
INSERT INTO ugset (ugsetid,hash) VALUES ('1','b21d61561974b06cbeb97a71ca880e92f0fc06d49ac3246280afb88dfa6050a6');
INSERT INTO users (userid,username,name,surname,passwd,url,autologin,autologout,refresh,rows_per_page,roleid) VALUES ('1','Admin','Zabbix','Administrator','$2y$10$92nDno4n0Zm7Ej7Jfsz8WukBfgSS/U0QkIuu8WkJPihXBb2A1UrEK','','1','0','30s','50','3'),
('2','guest','','','$2y$10$89otZrRNmde97rIyzclecuk6LwKAsHN0BcvoOKGjbT.BwMBfm7G06','','0','15m','30s','50','4');
INSERT INTO hstgrp (groupid,name,flags,uuid,type) VALUES ('1','Discovered hosts','0','f2481361f99448eea617b7b1d4765566','0');
INSERT INTO usrgrp (usrgrpid,name,gui_access,users_status,debug_mode,userdirectoryid,mfa_status,mfaid) VALUES ('7','Zabbix administrators','0','0','0',NULL,'0',NULL),
('8','Guests','0','0','0',NULL,'0',NULL),
('9','Disabled','0','1','0',NULL,'0',NULL),
('11','Enabled debug mode','0','0','1',NULL,'0',NULL),
('12','No access to the frontend','3','0','0',NULL,'0',NULL),
('13','Internal','1','0','0',NULL,'0',NULL);
INSERT INTO users_groups (id,usrgrpid,userid) VALUES ('2','8','2'),
('3','9','2'),
('4','7','1'),
('5','13','1'),
('6','13','2');
INSERT INTO ugset_group (ugsetid,usrgrpid) VALUES ('1','13'),
('1','8'),
('1','9');
INSERT INTO user_ugset (userid,ugsetid) VALUES ('2','1');
INSERT INTO config (configid,work_period,alert_usrgrpid,discovery_groupid,dbversion_status,geomaps_tile_provider,ldap_userdirectoryid,server_status,mfaid,software_update_check_data,default_lang) VALUES ('1','1-5,09:00-18:00','7','1','','OpenStreetMap.Mapnik',NULL,'',NULL,'','en_GB');
INSERT INTO graph_theme (graphthemeid,theme,backgroundcolor,graphcolor,gridcolor,maingridcolor,gridbordercolor,textcolor,highlightcolor,leftpercentilecolor,rightpercentilecolor,nonworktimecolor,colorpalette) VALUES ('1','blue-theme','FFFFFF','FFFFFF','CCD5D9','ACBBC2','ACBBC2','1F2C33','E33734','429E47','E33734','EBEBEB','1A7C11,F63100,2774A4,A54F10,FC6EA3,6C59DC,AC8C14,611F27,F230E0,5CCD18,BB2A02,5A2B57,89ABF8,7EC25C,274482,2B5429,8048B4,FD5434,790E1F,87AC4D,E89DF4'),
('2','dark-theme','2B2B2B','2B2B2B','454545','4F4F4F','4F4F4F','F2F2F2','E45959','59DB8F','E45959','333333','199C0D,F63100,2774A4,F7941D,FC6EA3,6C59DC,C7A72D,BA2A5D,F230E0,5CCD18,BB2A02,AC41A5,89ABF8,7EC25C,3165D5,79A277,AA73DE,FD5434,F21C3E,87AC4D,E89DF4'),
('3','hc-light','FFFFFF','FFFFFF','555555','000000','333333','000000','333333','000000','000000','EBEBEB','1A7C11,F63100,2774A4,A54F10,FC6EA3,6C59DC,AC8C14,611F27,F230E0,5CCD18,BB2A02,5A2B57,89ABF8,7EC25C,274482,2B5429,8048B4,FD5434,790E1F,87AC4D,E89DF4'),
('4','hc-dark','000000','000000','666666','888888','4F4F4F','FFFFFF','FFFFFF','FFFFFF','FFFFFF','333333','199C0D,F63100,2774A4,F7941D,FC6EA3,6C59DC,C7A72D,BA2A5D,F230E0,5CCD18,BB2A02,AC41A5,89ABF8,7EC25C,3165D5,79A277,AA73DE,FD5434,F21C3E,87AC4D,E89DF4');
INSERT INTO config_autoreg_tls (autoreg_tlsid,tls_psk_identity,tls_psk) VALUES ('1','','');
INSERT INTO module (moduleid,id,relative_path,status,config) VALUES ('1','actionlog','widgets/actionlog','1','[]'),
('2','clock','widgets/clock','1','[]'),
('3','topitems','widgets/topitems','1','[]'),
('4','discovery','widgets/discovery','1','[]'),
('5','favgraphs','widgets/favgraphs','1','[]'),
('6','favmaps','widgets/favmaps','1','[]'),
('7','geomap','widgets/geomap','1','[]'),
('8','graph','widgets/graph','1','[]'),
('9','graphprototype','widgets/graphprototype','1','[]'),
('10','hostavail','widgets/hostavail','1','[]'),
('11','item','widgets/item','1','[]'),
('12','map','widgets/map','1','[]'),
('13','navtree','widgets/navtree','1','[]'),
('14','itemhistory','widgets/itemhistory','1','[]'),
('15','problemhosts','widgets/problemhosts','1','[]'),
('16','problems','widgets/problems','1','[]'),
('17','problemsbysv','widgets/problemsbysv','1','[]'),
('18','slareport','widgets/slareport','1','[]'),
('19','svggraph','widgets/svggraph','1','[]'),
('20','systeminfo','widgets/systeminfo','1','[]'),
('21','tophosts','widgets/tophosts','1','[]'),
('22','trigover','widgets/trigover','1','[]'),
('23','url','widgets/url','1','[]'),
('24','web','widgets/web','1','[]'),
('25','gauge','widgets/gauge','1','[]'),
('26','toptriggers','widgets/toptriggers','1','[]'),
('27','piechart','widgets/piechart','1','[]'),
('28','honeycomb','widgets/honeycomb','1','[]'),
('29','hostnavigator','widgets/hostnavigator','1','[]'),
('30','itemnavigator','widgets/itemnavigator','1','[]'),
('31','hostcard','widgets/hostcard','1','[]');
INSERT INTO role_rule (role_ruleid,roleid,type,name,value_int,value_str,value_moduleid,value_serviceid) VALUES ('1','1','0','ui.default_access','1','',NULL,NULL),
('2','1','0','services.read','1','',NULL,NULL),
('3','1','0','services.write','0','',NULL,NULL),
('4','1','0','modules.default_access','1','',NULL,NULL),
('5','1','0','api.access','1','',NULL,NULL),
('6','1','0','api.mode','0','',NULL,NULL),
('7','1','0','actions.default_access','1','',NULL,NULL),
('8','2','0','ui.default_access','1','',NULL,NULL),
('9','2','0','services.read','1','',NULL,NULL),
('10','2','0','services.write','1','',NULL,NULL),
('11','2','0','modules.default_access','1','',NULL,NULL),
('12','2','0','api.access','1','',NULL,NULL),
('13','2','0','api.mode','0','',NULL,NULL),
('14','2','0','actions.default_access','1','',NULL,NULL),
('15','3','0','ui.default_access','1','',NULL,NULL),
('16','3','0','services.read','1','',NULL,NULL),
('17','3','0','services.write','1','',NULL,NULL),
('18','3','0','modules.default_access','1','',NULL,NULL),
('19','3','0','api.access','1','',NULL,NULL),
('20','3','0','api.mode','0','',NULL,NULL),
('21','3','0','actions.default_access','1','',NULL,NULL),
('22','4','0','ui.default_access','1','',NULL,NULL),
('23','4','0','services.read','1','',NULL,NULL),
('24','4','0','services.write','0','',NULL,NULL),
('25','4','0','modules.default_access','1','',NULL,NULL),
('26','4','0','api.access','0','',NULL,NULL),
('27','4','0','actions.default_access','0','',NULL,NULL);
INSERT INTO hgset (hgsetid,hash) VALUES ('1','e629fa6598d732768f7c726b4b621285f9c3b85303900aa912017db7617d8bdb'),
('2','4ec9599fc203d176a301536c2e091a19bc852759b255bd6818810a42c5fed14a'),
('3','4523540f1504cd17100c4835e85b7eefd49911580f8efff0599a8f283be6b9e3'),
('4','ef8704ac79657fbf2818c74a70a571c131283abd8914eca173cd032929702789'),
('5','6f4b6612125fb3a0daecd2799dfd6c9c299424fd920f9b308110a2c1fbd8f443'),
('6','4b227777d4dd1fc61c6f884f48641d02b4d121d3fd328cb08b5531fcacdabf8a'),
('7','19581e27de7ced00ff1ce50b2047e7a567c76b1cbaebabe5ef03f7c3017bb5b7'),
('8','6b51d431df5d7f141cbececcf79edf3dd861c3b4069f0b11661a3eefacbba918'),
('9','b17ef6d19c7a5b1ee83b907c595526dcb1eb06db8227d650d5dda0a9f4ce8cd9'),
('10','4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8'),
('11','4a44dc15364204a80fe80e9039455cc1608281820fe2b24f1e5233ade6af1dd5'),
('12','3fdba35f04dc8c462986c992bcf875546257113072a909c162f7e470e581e278');
INSERT INTO token (tokenid, name, description, userid, token, lastaccess, status, expires_at, created_at, creator_userid) VALUES (1, 'api', '', 1, '3369f8d8fe2b9e91ef6f947ec1a0d214a6e9732c44384516d79de61dd5047dd88dc3609c5ed1d5b7dd446b8e4bc7d32599e5343989a416cdfbcadd4e62dd637b', 0, 0, 0, 1669849200, 1);
DELETE FROM changelog;
COMMIT;
