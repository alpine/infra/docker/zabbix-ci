#!/bin/sh

# shellcheck disable=SC3040
set -eu -o pipefail

db() {
    PGPASSWORD="$DB_SERVER_PASS" psql \
        --host "${DB_SERVER_HOST}" \
        --port "${DB_SERVER_PORT}" \
        --username "${DB_SERVER_USER}" \
        --dbname "${DB_SERVER_DBNAME}" \
        "$@"
}

init_db() {
    echo "** Initializing database"

    for schema in /setup/db/*.sql; do
        echo "*** Applying $(basename "$schema")"
        db <"$schema"
    done
}

if db -c 'select * from dbversion' >/dev/null 2>&1; then
    echo "** DB already exists, skipping initialization"
else
    init_db
fi

echo "** Executing supervisord"
exec /usr/bin/supervisord -c /etc/supervisor/supervisord.conf
