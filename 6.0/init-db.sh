#!/bin/sh

# shellcheck disable=SC3040
set -eu -o pipefail

db() {
    PGPASSWORD="$DB_SERVER_PASS" psql \
        --host "${DB_SERVER_HOST}" \
        --port "${DB_SERVER_PORT}" \
        --username "${DB_SERVER_USER}" \
        --dbname "${DB_SERVER_DBNAME}" \
        "$@"
}

ids_query() {
    tablename=$1
    columnname=$2

    printf "INSERT INTO ids (table_name, field_name, nextid) VALUES ('%s', '%s', (SELECT MAX(%s) FROM %s));" "$tablename" "$columnname" "$columnname" "$tablename"
}

init_db() {
    echo "** Initializing database"

    for schema in /setup/db/*.sql; do
        echo "*** Applying $(basename "$schema")"
        db <"$schema"
    done

    # Prevent race condition when trying to create hostgroups in parallel on a new database
    echo "** Populating 'ids' table"
    db -e -c "$(ids_query hstgrp groupid)"
    db -e -c "$(ids_query usrgrp usrgrpid)"
    db -e -c "$(ids_query role roleid)"
    db -e -c "$(ids_query role_rule role_ruleid)"
    db -e -c "$(ids_query users userid)"
}

if db -c 'select * from dbversion' >/dev/null 2>&1; then
    echo "** DB already exists, skipping initialization"
else
    init_db
fi

echo "** Executing supervisord"
exec /usr/bin/supervisord -c /etc/supervisor/supervisord.conf
