package zabbix

type AgentDataItem struct {
	ID          int64  `json:"id"`
	Host        string `json:"host"`
	Key         string `json:"key"`
	Value       string `json:"value,omitempty"`
	LastLogSize int64  `json:"lastlogsize,omitempty"`
	Mtime       int64  `json:"mtime,omitempty"`
	Source      string `json:"source,omitempty"`
	EventID     int64  `json:"eventid,omitempty"`
	Severity    int64  `json:"severity"`
	Clock       int64  `json:"clock"`
	NS          int64  `json:"ns"`
}

type ServerResponseMessage struct {
	Response string `json:"response"`
	Info     string `json:"info"`
}
