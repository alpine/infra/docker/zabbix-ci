package zabbix

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"

	"github.com/rs/zerolog/log"
)

func decodeMsg(r io.Reader) ([]byte, *ZbxHeader, error) {
	headerData := make([]byte, 13)
	_, err := r.Read(headerData)
	if err != nil {
		return nil, nil, fmt.Errorf("error reading header: %w", err)
	}

	header := &ZbxHeader{}
	buf := bytes.NewBuffer(headerData)
	binary.Read(buf, binary.LittleEndian, header)

	log.Debug().Msgf("Received packet, protocol: %s, flags: 0x%x, len: %d", string(header.Protocol[:]), header.Flags, header.DataLen)

	requestData := make([]byte, header.DataLen)
	n, err := r.Read(requestData)
	if err != nil {
		return nil, nil, fmt.Errorf("error reading data: %w", err)
	}

	if n != int(header.DataLen) {
		log.Warn().Msgf("expected to receive %s bytes, actually received %s", header.DataLen, n)
	}

	return requestData, header, nil
}

func DecodeRequest(r io.Reader) (RequestMessage, *ZbxHeader, error) {
	requestData, header, err := decodeMsg(r)

	request := RequestMessage{}
	err = json.Unmarshal(requestData, &request)
	if err != nil {
		return RequestMessage{}, nil, fmt.Errorf("error decoding request: %w", err)
	}

	return request, header, nil
}

func DecodeServerResponse(r io.Reader) (ServerResponseMessage, *ZbxHeader, error) {
	requestData, header, err := decodeMsg(r)

	response := ServerResponseMessage{}
	err = json.Unmarshal(requestData, &response)
	if err != nil {
		return ServerResponseMessage{}, nil, fmt.Errorf("error decoding request: %w", err)
	}

	return response, header, nil
}

func encodeMsg(w io.Writer, msg any) error {
	responseData, err := json.Marshal(msg)

	responseHeader := ZbxHeader{
		Protocol: [4]byte{'Z', 'B', 'X', 'D'},
		Flags:    FLAG_ZBX,
		DataLen:  uint32(len(responseData)),
	}

	outBuf := bytes.Buffer{}
	err = binary.Write(&outBuf, binary.LittleEndian, responseHeader)
	if err != nil {
		return fmt.Errorf("error encoding response header: %w", err)
	}
	_, err = outBuf.Write(responseData)
	if err != nil {
		return fmt.Errorf("error writing response data: %w", err)
	}

	log.Debug().Msgf("outBuf: %s", hex.Dump(outBuf.Bytes()))

	_, err = w.Write(outBuf.Bytes())
	if err != nil {
		return fmt.Errorf("error writing reply: %w", err)
	}

	return nil
}

func EncodeResponse[T any](w io.Writer, msg ResponseMessage[T]) error {
	return encodeMsg(w, msg)
}

func EncodeAgentRequest(w io.Writer, msg AgentRequestMessage) error {
	return encodeMsg(w, msg)
}
