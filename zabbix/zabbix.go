package zabbix

const (
	FLAG_ZBX = 0x01 << iota
	FLAG_COMPRESSION
	FLAG_LARGE_PACKET
)

type ZbxHeader struct {
	Protocol [4]byte
	Flags    uint8
	DataLen  uint32
	Reserved uint32
}

type ResponseMessage[T any] struct {
	Response string `json:"response"`
	Data     T      `json:"data"`
}

func NewResponseMessage[T any](response string, data T) ResponseMessage[T] {
	return ResponseMessage[T]{
		Response: response,
		Data:     data,
	}
}

type RequestMessage struct {
	Request string `json:"request"`
	Type    string `json:"type"`
	Sid     string `josn:"sid"`
}

type AgentRequestMessage struct {
	Request string          `json:"request"`
	Data    []AgentDataItem `json:"data"`
}
