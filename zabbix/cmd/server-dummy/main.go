package main

import (
	"fmt"
	"net"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/pflag"
	"zabbix.local/zabbix"
	. "zabbix.local/zabbix"
)

func main() {
	port := pflag.StringP("port", "p", "10051", "Port to listen on, defaults to 10051")

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	log.Logger = log.Output(zerolog.ConsoleWriter{
		Out:        os.Stderr,
		TimeFormat: time.RFC3339,
	})

	listener, err := net.Listen("tcp", fmt.Sprintf(":%s", *port))
	if err != nil {
		log.Fatal().Err(err).Msgf("could not listen on port %s", *port)
	}

	log.Info().Msgf("Listening on :%s", *port)

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Error().Err(err).Msg("error accepting connection")
		}

		conn.SetDeadline(time.Now().Add(2 * time.Second))
		go handleConn(conn)
	}
}

func handleConn(conn net.Conn) {
	request, _, err := zabbix.DecodeRequest(conn)

	if err != nil {
		log.Error().Err(err).Msg("Error handling connection")
		return
	}

	if request.Request != "status.get" {
		log.Warn().Msgf("Received unknown request: %s", request.Request)
		return
	}

	log.Debug().Msgf("request: %s, type: %s", request.Request, request.Type)

	switch request.Type {
	case "full":
		response := StatusResponse()
		if err != nil {
			log.Error().Err(err).Msg("error encoding response")
			return
		}
		err = zabbix.EncodeResponse(conn, response)
		if err != nil {
			log.Error().Err(err).Msg("error sending response")
			return
		}
	case "ping":
		response := NewResponseMessage("success", []*struct{}{})
		if err != nil {
			log.Error().Err(err).Msg("error encoding response")
			return
		}
		err = zabbix.EncodeResponse(conn, response)
		if err != nil {
			log.Error().Err(err).Msg("error sending response")
			return
		}
	}

	log.Info().Msgf("received request %s, type %s. Sent reply", request.Request, request.Type)
}
