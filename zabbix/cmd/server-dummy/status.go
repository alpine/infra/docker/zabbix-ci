package main

import (
	. "zabbix.local/zabbix"
)

type templateStats struct {
	Count int32 `json:"count"`
}

type hostStats struct {
	Attributes hostStatAttributes `json:"attributes"`
	Count      int32              `json:"count"`
}

type hostStatAttributes struct {
	ProxyID int64 `json:"proxyid"`
	Status  int   `json:"status"`
}

type itemStats struct {
	Attributes itemStatAttributes `json:"attributes"`
	Count      int32              `json:"count"`
}

type itemStatAttributes struct {
	ProxyID int64 `json:"proxyid"`
	Status  int   `json:"status"`
	State   int   `json:"state"`
}

type triggerStats struct {
	Attributes triggerStatAttributes `json:"attributes"`
	Count      int32                 `json:"count"`
}

type triggerStatAttributes struct {
	Status int `json:"status"`
	Value  int `json:"value"`
}

type userStats struct {
	Attributes userStatAttributes `json:"attributes"`
	Count      int32              `json:"count"`
}

type userStatAttributes struct {
	Status int32 `json:"status"`
}

type performanceStats struct {
	Attributes performanceStatAttributes `json:"attributes"`
	Count      string                    `json:"count"`
}

type performanceStatAttributes struct {
	ProxyID int64 `json:"proxyid"`
}

type StatusMessage struct {
	TemplateStats []templateStats    `json:"template stats"`
	HostStats     []hostStats        `json:"host stats"`
	ItemStats     []itemStats        `json:"item stats"`
	TriggerStats  []triggerStats     `json:"trigger stats"`
	UserStats     []userStats        `json:"user stats"`
	Performance   []performanceStats `json:"required performance"`
}

func StatusResponse() ResponseMessage[StatusMessage] {
	return NewResponseMessage(
		"success",
		StatusMessage{
			TemplateStats: []templateStats{{Count: 0}},
			HostStats: []hostStats{
				{Attributes: hostStatAttributes{ProxyID: 0, Status: 0}, Count: 0},
			},
			ItemStats: []itemStats{
				{Attributes: itemStatAttributes{ProxyID: 0, Status: 0, State: 0}, Count: 0},
				{Attributes: itemStatAttributes{ProxyID: 0, Status: 0, State: 1}, Count: 0},
				{Attributes: itemStatAttributes{ProxyID: 0, Status: 1, State: 0}, Count: 0},
			},
			TriggerStats: []triggerStats{
				{Attributes: triggerStatAttributes{Status: 0, Value: 0}, Count: 0},
				{Attributes: triggerStatAttributes{Status: 0, Value: 1}, Count: 0},
				{Attributes: triggerStatAttributes{Status: 1, Value: 0}, Count: 0},
			},
			UserStats: []userStats{
				{Attributes: userStatAttributes{Status: 0}, Count: 0},
				{Attributes: userStatAttributes{Status: 1}, Count: 2},
			},
			Performance: []performanceStats{
				{Attributes: performanceStatAttributes{ProxyID: 0}, Count: "0"},
			},
		},
	)
}
