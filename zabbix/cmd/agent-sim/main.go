package main

import (
	"fmt"
	"io"
	"net"
	"os"
	"time"

	"gopkg.in/yaml.v3"
	"zabbix.local/zabbix"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/pflag"
)

func main() {
	server := pflag.StringP("server", "s", "", "Server to connect to (required)")
	host := pflag.StringP("host", "h", "agent-sim", "Hostname to report ot Zabbix, defaults to agent-sim")
	dataFile := pflag.StringP("data", "d", "", "location of the data file")
	port := pflag.StringP("port", "p", "10051", "Port to connect to, defaults to 10051")
	pflag.Parse()

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	log.Logger = log.Output(zerolog.ConsoleWriter{
		Out:        os.Stderr,
		TimeFormat: time.RFC3339,
	})

	if *server == "" {
		log.Fatal().Msg("No server specified")
	}

	if *dataFile == "" {
		log.Fatal().Msg("No data file specified")
	}

	f, err := os.Open(*dataFile)
	if err != nil {
		log.Fatal().
			Err(err).
			Str("filename", *dataFile).
			Msg("could not open file")
	}
	fileContents, err := io.ReadAll(f)
	if err != nil {
		log.Fatal().
			Err(err).
			Str("filename", *dataFile).
			Msg("error reading data file")
	}

	data := Schema{}
	err = yaml.Unmarshal(fileContents, &data)
	if err != nil {
		log.Fatal().Err(err).Msg("error decoding data file")
	}

	for _, i := range data.Items {
		log.Info().Str("key", i.Key).Int("items", len(i.Values)).Msg("")
	}

	ticker := time.NewTicker(1 * time.Minute)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			conn, err := net.Dial("tcp", fmt.Sprintf("%s:%s", *server, *port))
			if err != nil {
				log.Fatal().Err(err).Msg("Could not connect to server")
			}

			log.Info().Msgf("Sending data for %d items", len(data.Items))

			err = SendItems(conn, *host, data.Items)

			if err != nil {
				log.Error().Err(err).Msg("could not send data")
			}

			conn.Close()
		}
	}

}

func SendItems(conn net.Conn, host string, items []*Item) error {
	ts := time.Now()

	dataItems := make([]zabbix.AgentDataItem, 0, len(items))
	for _, i := range items {
		dataItems = append(dataItems,
			zabbix.AgentDataItem{
				Host:  host,
				Key:   i.Key,
				Value: i.Values[i.index],
				Clock: ts.Unix(),
				NS:    int64(ts.Nanosecond()),
			},
		)
		i.index = (i.index + 1) % len(i.Values)
	}

	err := zabbix.EncodeAgentRequest(conn, zabbix.AgentRequestMessage{
		Request: "agent data",
		Data:    dataItems,
	})

	if err != nil {
		return fmt.Errorf("could not send data: %w", err)
	}

	response, _, err := zabbix.DecodeServerResponse(conn)
	if err != nil {
		return fmt.Errorf("error decoding server response: %w", err)
	}

	log.Info().Str("info", response.Info).Msg(response.Response)
	return nil
}

type Schema struct {
	Items []*Item
}

type Item struct {
	Key    string
	Values []string
	index  int
}
